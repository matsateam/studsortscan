package net.ukr.wumf;

public class Main {

	public static void main(String[] args) {

		Student stud1 = new Student("������", "��������", 25," " , "1");
		Student stud2 = new Student("����", "������", 22," " , "2");
		Student stud3 = new Student("������", "��������", 23, "", "3");
		Student stud4 = new Student("�����", "��������", 22, "", "4");
		Student stud5 = new Student("����", "�������", 24, "", "5");
		Student stud6 = new Student("���", "���������", 21, "", "6");
		Student stud7 = new Student("������", "��������", 27, "", "7");
		Student stud8 = new Student("�����", "������", 23, "", "8");
		Student stud9 = new Student("���", "����", 22, "", "9");
		Student stud10 = new Student("����", "�����������", 21, "", "10");

		Group g10 = new Group("G10");
		Group g11 = new Group("G11");
		
		try {
			g10.addStudent(stud1);
			g10.addStudent(stud2);
			g10.addStudent(stud3);
			g10.addStudent(stud4);
			g10.addStudent(stud5);
			g10.addStudent(stud6);
			g10.addStudent(stud7);
			g10.addStudent(stud8);
			g10.addStudent(stud9);
			g10.addStudent(stud10);

		} catch (GroupIsFullException e) {
			e.printStackTrace();
		}
		
		System.out.println(g10);
		
		NewStudentScanner newStud = new NewStudentScanner();
		Student stud11 = newStud.readStudentFromfKeyboard();
		
		try {
			g11.addStudent(stud11);

		} catch (GroupIsFullException e) {
			e.printStackTrace();
		}
		
		System.out.println(stud11);
	}

}
