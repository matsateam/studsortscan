package net.ukr.wumf;

import java.util.Arrays;
import java.util.Comparator;

public class Group {
	private String nameGroup;
	private Student[] array = new Student[10];

	public Group(String nameGroup) {
		super();
		this.nameGroup = nameGroup;
	}

	public Group() {
		super();
	}

	public String getNameGroup() {
		return nameGroup;
	}

	public void setNameGroup(String nameGroup) {
		this.nameGroup = nameGroup;
	}

	public Student[] getArray() {
		return array;
	}

	public void setArray(Student[] array) {
		this.array = array;
	}

	public void addStudent(Student st) throws GroupIsFullException {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == null) {
				array[i] = st;
				array[i].setGroupName(nameGroup);
				break;
			}
			if (i == array.length - 1) {
				throw new GroupIsFullException("������ ��� ������");
			}
		}
	}

	public void deleteStudent(String id) {
		boolean check = true;
		for (int i = 0; i < array.length; i++) {
			if (array[i] != null) {
				if (id.equals(array[i].getId())) {
					array[i] = null;
					for (int k = i; k < array.length - 1; k++) {
						Student replace = array[k];
						array[k] = array[k + 1];
						array[k + 1] = replace;
					}
					check = false;
					break;
				}
			}
		}
		System.out.println((check) ? "������ �������� ��� � �������" : "������� c id " + id + " ������");
	}

	public Student findStudent(String lastName) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] != null) {
				if (lastName.equals(array[i].getLastName())) {
					return array[i];
				}
			}
		}
		return null;
	}

	public void sortStudentsByLastName(Student[] students) {
		Arrays.sort(students, Comparator.nullsFirst(new StudentLastNameComparator()));
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();

		sortStudentsByLastName(array);

		stringBuilder.append("\n");
		stringBuilder.append("������ "+ nameGroup + " : ");
		stringBuilder.append("\n");
		stringBuilder.append("\n");
		for (Student s : array) {
			if (s != null) {
				stringBuilder.append(s.getLastName() + " " + s.getName() + ", �������: " + s.getAge() + ";")
						.append("\n");
			}
		}
		stringBuilder.append("\n");

		return stringBuilder.toString();
	}

	
}